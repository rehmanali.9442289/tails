# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-08-29 15:15+0200\n"
"PO-Revision-Date: 2020-04-05 23:29+0000\n"
"Last-Translator: Muri Nicanor <muri@immerda.ch>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Content of: <p>
msgid ""
"It is currently impossible to manually upgrade a Tails USB stick while "
"running from itself. This scenario requires creating an intermediary Tails "
"on another USB stick, from which to upgrade your Tails."
msgstr ""
"Es ist derzeit nicht möglich, Tails manuell vom selben USB-Stick zu "
"aktualisieren, von dem es läuft. In diesem Fall ist es notwendig, ein "
"zwischenzeitliches Tails auf einem anderen USB-Stick zu installieren, von "
"dem aus Sie Ihr Tails aktualisieren können."
