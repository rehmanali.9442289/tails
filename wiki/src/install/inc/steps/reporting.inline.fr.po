# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-08-29 08:06+0000\n"
"PO-Revision-Date: 2020-01-24 08:26+0000\n"
"Last-Translator: takt <ffr43366@eveav.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Content of: <h3>
msgid "Reporting the problem to our help desk"
msgstr ""

#. type: Content of: <p>
msgid "Please send the following information to [[tails-support-private@boum.org]]:"
msgstr ""
"Merci d'envoyer les informations suivantes à [[tails-support-"
"private@boum.org]] :"

#. type: Content of: <ol><li><p>
msgid "Which version of Tails are you trying to start?"
msgstr "Quelle version de Tails essayez-vous de démarrer ?"

#. type: Content of: <ol><li><p>
msgid "How did you verify the USB image?"
msgstr "Comment avez-vous vérifié l'image USB ?"

#. type: Content of: <ol><li><p>
msgid "What is the brand and model of your computer?"
msgstr "Quels sont la marque et le modèle de votre ordinateur ?"

#. type: Content of: <ol><li><p>
msgid ""
"What exactly happens when trying to start? Report the complete error message "
"that appears on the screen, if any."
msgstr ""

#. type: Content of: <ol><li><p>
msgid "What program did you use to install your USB stick?"
msgstr ""
"Quel programme avez-vous utilisé pour installer Tails sur votre clé USB ?"

#. type: Content of: <ol><li><p>
msgid ""
"Does the USB stick start successfully on other computers? If so, what are "
"the brands and models of those computers?"
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"Have you been able to successfully start another version of Tails on this "
"computer before? If so, which version?"
msgstr ""
"Avez-vous déjà pu démarrer correctement une autre version de Tails sur cet "
"ordinateur ? Si oui, quelle version ?"

#. type: Content of: outside any tag (error?)
msgid "[[!inline pages=\"support/talk/languages.inline\" raw=\"yes\"]]"
msgstr "[[!inline pages=\"support/talk/languages.inline.fr\" raw=\"yes\"]]"
